from keras.models import load_model
from keras.models import model_from_json

model = load_model("emotion_model.hdf5")
model_json = model.to_json()
with open("emotion_model.json", 'w') as json_file:
	json_file.write(model_json)

model.save_weights("model.h5")
