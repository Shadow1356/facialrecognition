# Some Code and Training Data Modified (stolen) from https://github.com/petercunha/Emotion
print(__name__)
print("Setting up OpenCV")
import cv2
print("Loading Keras Model")
from keras.models import model_from_json
print("Finished Keras Model")
import os
print("Importing Numpy functions")
from numpy import array
from numpy import expand_dims
from numpy import argmax
from numpy import arange
print("Importing rounding and random functions")
from random import randint
from math import ceil
print("Executing main Program")


PREVIEW_WIDTH = 600
PREVIEW_HEIGHT = 400
#Size of the preview window in the GUI
SAMPLE = 10
#This is the number frames it waits (per default) to check for new faces

emotion_model_path = 'emotion_model.hdf5'
emotion_labels = {0:'angry',1:'disgust',2:'fear',3:'happy',
                4:'sad',5:'surprise',6:'neutral'}
#loading the emotion models
face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
with open("emotion_model.json", 'r') as json_file:
	loaded_json = json_file.read()
emotion_classifier = model_from_json(loaded_json)
emotion_classifier.load_weights("model.h5")
emotion_target_size = emotion_classifier.input_shape[1:3]
emotion_offsets = (20, 40)
#These functions process image arrays so that the model can read them. 
def preprocess_input(x, v2=True):
    x = x.astype('float32')
    x = x / 255.0
    if v2:
        x = x - 0.5
        x = x * 2.0
    return x
def detect_faces(detection_model, gray_image_array):
    return detection_model.detectMultiScale(gray_image_array, 1.3, 5)

def draw_bounding_box(face_coordinates, image_array, color):
	#draws rectangle on screen
    x, y, w, h = face_coordinates
    cv2.rectangle(image_array, (x, y), (x + w, y + h), color, 2)

def apply_offsets(face_coordinates, offsets):
    x, y, width, height = face_coordinates
    x_off, y_off = offsets
    return (x - x_off, x + width + x_off, y - y_off, y + height + y_off)

def draw_text(coordinates, image_array, text, color, x_offset=0, y_offset=0,
                                                font_scale=2, thickness=2):
	# function to make drawing text on screen "easier"
    x, y = coordinates[:2]
    cv2.putText(image_array, text, (x + x_offset, y + y_offset),
                cv2.FONT_HERSHEY_SIMPLEX,
                font_scale, color, thickness, cv2.LINE_AA)
def is_intersecting(rect1, rect2):
	#check if 2 rectangles are intersecting
	# General Algorithm from: https://stackoverflow.com/questions/25068538/intersection-and-difference-of-two-rectangles/25068722#25068722
	print(rect1, "\t", rect2)
	x1 = max(rect1[0], rect2[0])
	x2 = min(rect1[0] + rect1[2], rect2[0] + rect2[2])
	y1 = max(rect1[1], rect2[1])
	y2 = min(rect1[1] + rect1[3], rect2[1] + rect2[3])
	print("Calculated values = ", x1, " ", x2, " ", y1, " ", y2)
	if x1<x2 and y1<y2:
		return True
	return False

class EmotionClassifier():
	#This class does all the processing of the frames/images
	#It will send the modified frames to the GUI (face_gui.py)
	def __init__(self):
		self.raw_image = None
		self.preview_frame = None
		self.capture = None
		self.x_scale = 1
		self.y_scale =1
		self.rotation_var = None
		self.flip_var = None
		self.location_path = ""
		self.f_type = 0
		self.directory = False
		# self.emotion_count = {'angry':0, 'disgust':0, 'fear':0, 'happy':0, 'sad':0,
  #               'surprise':0, 'neutral':0}
		self.analyze = False
		self.gamma = 1.0
		self.save_image = False
		self.save_file_path = ""
		self.save_video = False
		self.video_writer = None
		self.frame_width = 0
		self.frame_height = 0
		self.trackers = {}
		self.no_face = True
		self.FAIL_TIME_THRESHOLD = 2 #seconds
		self.failure_threshold = 4 #will be changed later in program
		self.analysis_data = []

	def calculate_failure_threshold(self, FPS):
		#This is threshold of how many times it doesn't find a face near it before it discards it. Change this as neccessary.
				# 3 => ~20 frames of no faces detected
		total_frames = FPS * self.FAIL_TIME_THRESHOLD
		float_frames = float(total_frames/10.0)
		self.failure_threshold = ceil(float_frames) - 1
		print("setting failure threshold at ", self.failure_threshold)

	def apply_transformations(self):
		# Does all the roatating, mirroring, etc. to the preview image.
		if not self.rotation_var is None:
			# print("Trying to rotate")
			self.raw_image = cv2.rotate(self.raw_image, self.rotation_var)
		if not self.flip_var is None:
			print("Trying to mirror")
			self.raw_image = cv2.flip(self.raw_image, self.flip_var)
		if self.gamma != 1.0:
			gamma_multiplier = 1.0/self.gamma
			lookup_table = array([((i/255.0)**gamma_multiplier) * 255 
										for i in arange(0,256)]).astype("uint8")
			self.raw_image = cv2.LUT(self.raw_image, lookup_table)



	def process_file(self, path=None, file_type=None, isDir=None):
		#The start of the processing process.
		print("Resetting Trackers")
		del self.trackers
		self.trackers = {}
		self.no_face = True
		if path is None:
			path = self.location_path
			file_type=self.f_type
			isDir = self.directory
		self.location_path = path
		self.f_type = file_type
		self.directory = isDir
		print(path)
		print(file_type)
		print(isDir)
		if not isDir:
			if file_type == 0:
				# process a single image
				self.raw_image = cv2.imread(path)
				self.frame_height, self.frame_width = self.raw_image.shape[:2]
				self.apply_transformations()
				self.__generate_preview()
				self.classify_frame(0)
				yield self.preview_frame
			else:
				# Process some video stream
				# the path variable is set to differentiate which one.
				# 0 or 1 ==> Webcam
				# filepath ==> File
				print("trying to capture")
				try:
					#Release any current captures first.
					self.capture.release()
				except AttributeError:
					pass
				self.capture = cv2.VideoCapture(path)
				if not self.capture.isOpened() or self.capture is None:
					# sometimes it gets stuck. just wait a second and try again.
					print("trying to reset capture")
					self.capture.release()
					self.capture = cv2.VideoCapture(path)
					print("Have a new capture")
				# get the fps information so that Time To Fail can be calculated.
				frame_count = 0
				FPS = self.capture.get(cv2.CAP_PROP_FPS)
				print("FPS = ", FPS)
				self.calculate_failure_threshold(FPS)
				self.frame_width = self.capture.get(3)
				self.frame_height = self.capture.get(4)
				while self.capture.isOpened():
					# print("Inside the Capture Loop")
					flag, self.raw_image = self.capture.read()
					if not flag:
						break
					# Frame size gets were here
					self.apply_transformations()
					self.classify_frame(frame_count)
					frame_count += 1
					# print("At yield")
					yield self.preview_frame

	def clean_video_writer(self):
		# Used at the end to completely clean the video readers.
		self.video_writer.release()
		self.video_writer = None
		self.stop_analysis(self.save_file_path)

	def __generate_preview(self):
		# readies the preview image to be shown in the window.
		if self.save_image:
			# save it is the button was pressed.
			ext = self.save_file_path + ".jpg"
			cv2.imwrite(ext, self.raw_image)
			# self.save_image = False
		if self.save_video:
			# save the video if currently saving/analyzing.
			codec = cv2.VideoWriter_fourcc(*'DIVX')
			ext = self.save_file_path + ".avi"
			if self.video_writer is None:
				if self.rotation_var == cv2.ROTATE_90_CLOCKWISE or self.rotation_var == cv2.ROTATE_90_COUNTERCLOCKWISE:
					size = (int(self.frame_height), int(self.frame_width))
				else:
					size = (int(self.frame_width), int(self.frame_height))
				self.video_writer = cv2.VideoWriter(ext, codec, 20.0, size)
			self.video_writer.write(self.raw_image)
		self.preview_frame = self.raw_image
		self.preview_frame = cv2.resize(self.preview_frame, (PREVIEW_WIDTH, PREVIEW_HEIGHT))
		self.preview_frame = cv2.cvtColor(self.preview_frame, cv2.COLOR_BGR2RGB)

	def rotate(self):
		print("in rotate function")
		rotations = [None, cv2.ROTATE_90_CLOCKWISE, cv2.ROTATE_180, cv2.ROTATE_90_COUNTERCLOCKWISE]
		current_index = rotations.index(self.rotation_var)
		self.rotation_var = rotations[(current_index+1)%4]

	def mirror(self):
		print("in mirror function")
		if self.flip_var is None:
			self.flip_var = 1
		else:
			self.flip_var = None

	def set_gamma(self, brighten): #This is ugliness
		if brighten:
			k = 1.0
		else:
			k = -1.0
		if self.gamma < 1.0:
			k = k * .1
		elif self.gamma == 1.0:
			if k < 0:
				k = k*.1
		self.gamma += k
		print("New Gamma : ", self.gamma)


	def __del__(self):
		try:
			cv2.destroyAllWindows()
			self.capture.release()
			self.video_writer.release()
		except Exception:
			pass

	def classify_frame(self, frame_count):
		# The "meat and potatoes"
		#make the image grey and decide whether we need to track it or do detection.
		gray_image = cv2.cvtColor(self.raw_image, cv2.COLOR_BGR2GRAY)
		if (frame_count % SAMPLE == 0) or self.no_face: #if it's a certain frame number, update with detection information
			print("Trying to detect Face. Frame # ", frame_count)
			faces = face_cascade.detectMultiScale(gray_image, 1.3, 5)
			if len(faces) == 0:
				print("Found no faces")
				self.no_face = True
				self.update_trackers(gray_image)
				for tracker_id, tracker_data in self.trackers.items():
					#This shouldn't be too much of a performance hit since it's fast
					#and if this executes, the below loop won't (because len(faces) == 0)
					if not tracker_data[2]:
						continue
					self.trackers[tracker_id][4] += 1
			for face_coordinates in faces:
				draw_bounding_box(face_coordinates, self.raw_image, (0, 0, 0))
				self.no_face = False
				face_tuple = tuple((i for i in face_coordinates))
				# if frame_count % SAMPLE == 0:
				for tracker_id, tracker_data in self.trackers.items():
					if not tracker_data[2]:
						continue
					self.trackers[tracker_id][4] += 1
					# print(" From upper If, Testing Coordinates : ", tracker_data[0], "  Tracker #", tracker_id)
					int_test = is_intersecting(face_coordinates, tracker_data[0])
					print("IS Intersecting: ", int_test)
					if int_test: # detected and tracking the same face (probably)
						new_tracker = cv2.TrackerCSRT_create()
						new_tracker.init(gray_image, face_tuple)
						color = tracker_data[3]
						tracker_data =[face_coordinates, new_tracker, True, color, 0]
						self.trackers[tracker_id] = tracker_data
						break
				else:
					# It did not find an intersection, so we've found a new face
					new_tracker = cv2.TrackerCSRT_create()
					new_tracker.init(gray_image, face_tuple)
					try:
						new_key = max(self.trackers.keys()) + 1
					except ValueError:
						# This is the first face detected
						new_key = 1
					color = (randint(0, 255), randint(0, 255), randint(0, 255))
					self.trackers[new_key] = [face_coordinates, new_tracker, True, color, 0]
		else:
			#Just track using tracking algorithm.
			self.update_trackers(gray_image)

		index = 0
		if self.analyze:
			self.analysis_data.append({})
		last_analysis_index = len(self.analysis_data)-1
		for tracker_id, tracker_data in self.trackers.items():
			# print("Testing Coordinates : ", tracker_data[0], "  Tracker #", tracker_id)
			if not tracker_data[2]:
				continue
			id_string = str(tracker_id)
			face_coordinates = tracker_data[0]
			color = tracker_data[3]
			x1, x2, y1, y2 = apply_offsets(face_coordinates, emotion_offsets)
			# print("Coord", face_coordinates, type(face_coordinates))
			# test_coords = (120, 140, 150, 150)
			# draw_bounding_box(test_coords, self.raw_image, (255, 255, 0))
			gray_face = gray_image[int(y1):int(y2), int(x1):int(x2)]

			try:
				gray_face = cv2.resize(gray_face, (emotion_target_size))
			except Exception as e:
				# print(e)
				old_x1, old_y1, old_width, old_height = face_coordinates
				old_x2 = old_x1 + old_width
				old_y2 = old_y1 + old_height
				gray_face = gray_image[int(old_y1):int(old_y2), int(old_x1):int(old_x2)]
				# print("Non-resized : ", gray_face)
			finally:
				index += 1
			try:
				gray_face = cv2.resize(gray_face, (emotion_target_size))
				# cv2.imshow('gray_image', gray_face)
				# cv2.waitKey(5)
			except:
				print("rectangle not on screen")
				if self.analyze:
					frame_dict = self.analysis_data[last_analysis_index]
					frame_dict[tracker_id] = [False, []]
				continue
			gray_face = preprocess_input(gray_face, True)
			gray_face = expand_dims(gray_face, 0)
			gray_face = expand_dims(gray_face, -1)
			emotion_prediction = emotion_classifier.predict(gray_face)
			emotion_label_arg = argmax(emotion_prediction)
			print(emotion_prediction)
			probability = int(emotion_prediction[0][emotion_label_arg] * 100)
			emotion_text = emotion_labels[emotion_label_arg] + " " + str(probability) + "%"
			draw_bounding_box(face_coordinates, self.raw_image, color)
			draw_text(face_coordinates, self.raw_image, emotion_text,
		                  (0,0,255), 0, 0, 1, 2)
			id_coordinates = face_coordinates
			id_coordinates[1] += 35
			# print(id_string)
			draw_text(id_coordinates, self.raw_image, id_string, (0,0,255), 0, 0, 1, 2)
			if self.analyze:
				print("Analyzing Frame")
				frame_dict = self.analysis_data[last_analysis_index]
				print(emotion_prediction)
				frame_dict[tracker_id] = [True, emotion_prediction]
		self.__generate_preview()

	def update_trackers(self, gray_image):
		for tracker_id, tracker_info in self.trackers.items():
			if not tracker_info[2]:
				continue
			print("Tracker #", tracker_id, "has failure value of ", tracker_info[4])
			if tracker_info[4] >= self.failure_threshold: 
				self.trackers[tracker_id][2] = False
				continue
			tracker = tracker_info[1]
			retval, corrds_return = tracker.update(gray_image)
			int_coords = [int(i) for i in corrds_return]
			face = array(int_coords)
			print("Tracked Face at : ", face)
			self.no_face = True
			if retval:
				self.no_face = self.no_face and False
			else:
				self.no_face = self.no_face and True
				print("Lost track of a face.")
			tracker_info[2] = retval
			tracker_info[0] = face
			self.trackers[tracker_id] = tracker_info

	def stop_analysis(self, save_path):
		analysis_str = str(self.analysis_data)
		path_ext = save_path +".data"
		with open(path_ext, 'w') as dump:
			dump.write(analysis_str)
		self.analyze = False
		del self.analysis_data
		self.analysis_data = []
		print("Analysis Written successfully")
					
	def process_directory(self):
		for picture in os.listdir("EmotionTestingDir"):
			location = "EmotionTestingDir/" + picture
			image = cv2.imread(location)

			result_location = "EmotionTestingResults/" + picture
			cv2.imwrite(result_location, image)
