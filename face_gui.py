from tkinter import Tk, Button, Label, IntVar, Radiobutton, Frame, Entry
from PIL import Image
from PIL import ImageTk
from tkinter.filedialog import askopenfilename, asksaveasfilename
import face

class GUI(Frame):
	def __init__(self, master):
		super().__init__(master)
		master.option_add("*Font", ("Arial", 15))
		master.geometry("1050x650")
		master.title("Emotional Detection - Ashland University")
		self.preview = Label(master)
		self.bright_button = Button(master, text="Brighten")
		self.dark_button = Button(master, text="Darken")
		self.rotate_button = Button(master, text="Rotate")
		self.flip_button = Button(master, text="Mirror")
		self.file_upload_button = Button(master, text="Choose Picture/Video")
		self.video_play_button = Button(master, text="Play")
		self.video_pause_button = Button(master, text="Pause")
		self.video_restart_button = Button(master, text="Restart")
		self.record_button = Button(master, text="Save Video")
		self.start_analysis_button = Button(master, text="Start Analysis")
		self.pause_analysis_button = Button(master, text="Pause Analysis")
		self.stop_analysis_button = Button(master, text="Stop Analysis")
		self.save_file_label = Label(master, text="emotion_stats.txt")
		self.save_file_full_path = "emotion_stats.txt"
		self.change_file_button = Button(master, text="Choose File")
		self.save_picture_button =Button(master, text="Save Image")
		self.input_type_var = IntVar()
		self.picture_option = Radiobutton(master, text="Picture", variable=self.input_type_var, value=0)
		self.video_option = Radiobutton(master, text="Video", variable=self.input_type_var, value = 1)
		self.webcam_option = Radiobutton(master, text="Camera", variable=self.input_type_var, value =2)
		self.time_entry = Entry(master, width=4)
		self.switch_camera_button = Button(master, text="Switch Webcam")
		self.time_button = Button(master, text="Set Time to Fail")
		self.more_image = False
		self.image_generator = None
		self.camera_id = 0
		self.detector = face.EmotionClassifier()
		self.place_gui()
		self.initialize_preview()
		self.add_functionality()
		self.do_loop()


	def do_loop(self):
		while True:
			self.master.update()
			if self.more_image:
				self.show_preview()

	def save_image(self):
		# self.pause()
		new_location = asksaveasfilename(title="Choose File")
		print(new_location)
		if new_location == () or new_location == "": #User cancelled
			return
		self.detector.save_file_path = new_location
		self.detector.save_image = True
		self.restart()
		self.detector.save_image = False
		# self.play()


	def save_video(self):
		if not self.detector.save_video:
			new_location = asksaveasfilename(title="Choose File")
			print(new_location)
			if new_location == () or new_location == "": #User cancelled
				return
			self.save_picture_button.configure(state="disabled")
			self.detector.save_file_path = new_location
			self.record_button.configure(bg="red")
			self.detector.save_video = True
			self.detector.analyze = True
			# self.restart()
		else:
			normal_color = self.stop_analysis_button.cget("background")
			self.record_button.configure(bg=normal_color)
			self.detector.save_video = False
			self.detector.clean_video_writer()
			self.save_picture_button.configure(state="normal")



	def initialize_preview(self):
		gen = self.detector.process_file("preview_start.jpg", 0, False)
		cv_image = next(gen)
		PIL_image = Image.fromarray(cv_image)
		tk_image = ImageTk.PhotoImage(PIL_image)
		self.preview.configure(image=tk_image)
		self.preview.image=tk_image
		try:
			next(gen)
		except StopIteration:
			print("Finished getting preview initialized")

	def place_gui(self):
		self.preview.grid(row=0, column=0, columnspan=10, rowspan=10)
		self.bright_button.grid(row=1, column=11)
		self.dark_button.grid(row=1, column=12)
		self.rotate_button.grid(row=5, column= 11)
		self.flip_button.grid(row=5, column=12)
		self.video_restart_button.grid(row=9, column=11)
		self.video_pause_button.grid(row=9, column=12)
		self.video_play_button.grid(row=9, column=13)
		self.file_upload_button.grid(row=11, column=1)
		self.picture_option.grid(row=11, column=3)
		self.video_option.grid(row=11, column=5)
		self.webcam_option.grid(row=11, column=7)
		self.save_picture_button.grid(row=11, column=9)
		self.record_button.grid(row=11, column=11)
		Label(self.master, text="Time To Fail(seconds)").grid(row=12, column=1)
		self.time_entry.grid(row=12, column=2)
		self.time_entry.insert(0, str(self.detector.FAIL_TIME_THRESHOLD))
		self.switch_camera_button.grid(row=12, column=9)
		self.time_button.grid(row=13, column=1)
		# self.start_analysis_button.grid(row=12, column=1, pady=10)
		# self.pause_analysis_button.grid(row=13, column=1, pady=10)
		# self.stop_analysis_button.grid(row=14, column=1, pady=10)
		# self.save_file_label.grid(row=12, column=5, columnspan=2)
		# self.change_file_button.grid(row=12, column=7)
		
	def play(self):
		self.more_image = True

	def add_functionality(self):
		self.file_upload_button.configure(command=lambda:self.upload_file(self.input_type_var.get(), False))
		self.rotate_button.configure(command=self.rotate)
		self.video_play_button.configure(command=self.play)
		self.video_pause_button.configure(command=self.pause)
		self.video_restart_button.configure(command=self.restart)
		self.webcam_option.configure(command=self.start_camera)
		self.start_analysis_button.configure(command=self.start_analysis)
		self.pause_analysis_button.configure(command=self.pause_analysis)
		self.stop_analysis_button.configure(command=self.stop_analysis)
		self.change_file_button.configure(command=lambda:self.change_file_path(self.save_file_label))
		self.flip_button.configure(command=self.mirror)
		self.bright_button.configure(command=self.brighten)
		self.dark_button.configure(command=self.darken)
		self.save_picture_button.configure(command=self.save_image)
		self.record_button.configure(command=self.save_video)
		self.time_button.configure(command=self.set_TTF)
		self.switch_camera_button.configure(command=self.switch_camera)


	def switch_camera(self):
		self.camera_id = (self.camera_id + 1) % 2 # it will alternate between 0 and 1
		if self.input_type_var.get() == 2:
			self.start_camera()

	def set_TTF(self):
		str_input = self.time_entry.get()
		try:
			TTF_value = float(str_input)
		except ValueError:
			print("not a valid number.")
			return
		self.detector.FAIL_TIME_THRESHOLD = TTF_value
		self.restart()


	def brighten(self):
		self.detector.set_gamma(True)
		self.restart()

	def darken(self):
		self.detector.set_gamma(False)
		self.restart()


	def start_analysis(self):
		self.detector.analyze = True
		normal_color = self.stop_analysis_button.cget("background")
		self.start_analysis_button.configure(bg="red")
		self.pause_analysis_button.configure(bg=normal_color)

	def change_file_path(self, label):
		new_location = asksaveasfilename(title="Choose File")
		print(new_location)
		if new_location == () or new_location == "": #User cancelled
			return
		self.save_file_full_path = new_location
		file_name = new_location.split("/")[-1]
		print(file_name)
		label.config(text=file_name)

	def pause_analysis(self):
		self.detector.analyze = False
		normal_color = self.stop_analysis_button.cget("background")
		self.pause_analysis_button.configure(bg="red")
		self.start_analysis_button.configure(bg=normal_color)

	def stop_analysis(self):
		normal_color = self.stop_analysis_button.cget("background")
		self.detector.stop_analysis(self.save_file_full_path)
		self.start_analysis_button.configure(bg=normal_color)
		self.pause_analysis_button.configure(bg=normal_color)

	def start_camera(self):
		self.image_generator = self.detector.process_file(self.camera_id, 2, False)
		self.show_preview()

	def pause(self):
		self.more_image = False

	def restart(self):
		self.image_generator = self.detector.process_file()
		self.show_preview()

	def mirror(self):
		self.detector.mirror()
		self.restart()

	def rotate(self):
		self.detector.rotate()
		self.restart()

	def show_preview(self):
		try:
			# print("In Update")
			cv_image = next(self.image_generator)
			PIL_image = Image.fromarray(cv_image)
			tk_image = ImageTk.PhotoImage(PIL_image)
			self.preview.configure(image=tk_image)
			self.preview.image=tk_image
		except StopIteration:
			print("end of video/capture")
			self.more_image = False

	def upload_file(self, file_type, isDir, file=None):
		try:
			if file is None:
				location = askopenfilename(title="Choose File")
				if location == () or location == "": #User cancelled
					return
			else:
				location = file
			self.image_generator =self.detector.process_file(location, file_type, isDir)
			self.show_preview()
			print(location)
		except StopIteration:
			print("Generator has stopped")

if __name__ == "__main__":
	 root = Tk()
	 gui = GUI(root)
