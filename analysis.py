from tkinter import Tk, Button, Label, IntVar, Frame, PhotoImage, Listbox, END, font, Scrollbar, VERTICAL
from PIL import Image
from PIL import ImageTk
from tkinter.filedialog import askopenfilename, asksaveasfilename
from analysis_player import VideoProcessor
from numpy import array
from numpy import float32
from numpy import argmax
import pprint
from csv import DictWriter
class GUI(Frame):
	def __init__(self, master):
		super().__init__(master)
		master.option_add("*Font", ("Arial", 15))
		# master.attributes("-fullscreen", True)
		master.geometry("1300x700")
		master.title("Emotion Analysis - Ashland University")
		self.preview = Label(master)
		self.file_upload_button = Button(master, text="Choose Video")
		self.video_play_button = Button(master, text="Play")
		self.video_pause_button = Button(master, text="Pause")
		self.video_restart_button = Button(master, text="Restart")
		self.video_next_frame_button = Button(master, text=">>")
		self.video_prev_frame_button = Button(master, text="<<")
		# self.save_file_label = Label(master, text="emotion_stats.txt")
		# self.save_file_full_path = "emotion_stats.txt"
		self.change_file_button = Button(master, text="Download")
		self.delete_button = Button(master, text="Delete")
		self.delete_before_button = Button(master, text="Delete Before Frame")
		self.delete_after_button = Button(master, text="Delete After Frame")
		self.merge_button = Button(master, text="Merge")
		self.original_button = Button(master, text="See Original Size")
		self.emotion_frame = Frame(master, height=5, width=5)
		self.list_scroller = Scrollbar(master, orient=VERTICAL, troughcolor='black')
		self.tracker_list = Listbox(self.emotion_frame, width=12, selectmode="multiple", yscrollcommand=self.list_scroller.set)
		self.emotion_lists = [Listbox(self.emotion_frame, width=5, height=10, yscrollcommand=self.list_scroller.set) for i in range(0, 7)]
		self.emotion_names = ["Anger", "Disgust", "Fear", "Happy", "Sad", "Surprise", "Neutral"]
		self.current_trackers = Label(master, text="All")
		self.total_frame_label = Label(master)
		self.uptime_label = Label(master)
		self.gui_frame_number = IntVar()
		self.frame_number_label = Label(master, textvariable=self.gui_frame_number)
		self.emotion_labels = [Label(master) for i in range(0,7)]
		self.video_file = None
		self.data_file = None
		self.video_player = None
		self.more_image = False
		self.video_generator = None
		self.previous_trackers = []
		self.data = []
		self.stats_data = {}
		self.place_gui()
		self.initialize_preview()
		self.add_functionality()
		self.do_loop()

	def import_data(self):
		try:
			with open(self.data_file, 'r') as file:
				self.data = eval(file.read())
				# pprint.pprint(self.data)
			self.translate_keys()
			self.sort_data()
			self.update_stats(None)
		except FileNotFoundError:
			print("No data file found")
			del self.video_player
			del self.video_generator


	def translate_keys(self):
		new_data = []
		for frame in range(0, len(self.data)):
			print(frame)
			new_data.append({})
			for tracker_id, tracker_prob in self.data[frame].items():
				print(self.data[frame])
				print(self.data[frame][tracker_id])
				new_data[frame][str(tracker_id)] = tracker_prob
		self.data = new_data


	def sort_data(self): # This function gets the .data file into an easier to use format for the Reporting
		self.stats_data["total"] = len(self.data)
		self.stats_data["trackers"] = {}
		for frame in self.data:
			for tracker_id, tracker_probabilities in frame.items():
				if not str(tracker_id) in self.stats_data["trackers"]:
					self.stats_data["trackers"][str(tracker_id)] = {"total" : 1}
					for name in self.emotion_names:
						self.stats_data["trackers"][str(tracker_id)][name] = 0
				else:
					if tracker_probabilities[0]:
						self.stats_data["trackers"][str(tracker_id)]["total"] += 1
				if tracker_probabilities[0]:
					emotion_index = argmax(tracker_probabilities[1])
					name = self.emotion_names[emotion_index]
					self.stats_data["trackers"][str(tracker_id)][name] += 1

		with open("dump", 'w') as dump:
			pprint.pprint(self.stats_data, stream=dump)

	def update_stats(self, event):
		indexs = self.tracker_list.curselection()
		listbox_strs = self.tracker_list.get(0, END)
		tracker_map = {}
		for index in range(0, len(listbox_strs)):
			tracker_map[index] = listbox_strs[index]
		print("Map ", tracker_map)
		print("Selected : ", indexs)
		self.total_frame_label.config(text=str(self.stats_data["total"]))
		uptime = 0
		uptime_denominator =  0
		emotion_numerators = [0 for i in range(0, 7)]
		if len(indexs) == 0:
			indexs = list(self.stats_data["trackers"].keys())
			selected_str = "All"
		else:
			selected_str = str(indexs)
		self.current_trackers.config(text=selected_str)
		for tracker in indexs:
			try:
				mapped_tracker = tracker_map[tracker]
			except KeyError:
				print("Could not find tracker #", tracker, " While updating stats.")
				mapped_tracker = tracker
			tracker_stats = self.stats_data["trackers"][mapped_tracker]
			print(tracker_stats["total"])
			uptime += tracker_stats["total"]
			uptime_denominator += self.stats_data["total"]
			for i in range(0, 7):
				emotion_numerators[i] += self.stats_data["trackers"][mapped_tracker][self.emotion_names[i]]
		uptime_percentage = (uptime/uptime_denominator) *100
		uptime_str = "{:.2f}".format(uptime_percentage)
		self.uptime_label.config(text=uptime_str)
		for index in range(0, 7):
			numerator = emotion_numerators[index]
			percentage = (numerator/uptime) * 100
			percentage_str = "{:.2f}".format(percentage)
			self.emotion_labels[index].config(text=percentage_str)


	def do_loop(self):
		while True:
			self.master.update()
			try:
				self.gui_frame_number.set(self.video_player.current_frame +1)
			except:
				pass #video player hasn't been created.
			if self.more_image:
				self.show_preview()

	def show_preview(self):
		try:
			cv_image = next(self.video_generator)
			PIL_image = Image.fromarray(cv_image)
			tk_image = ImageTk.PhotoImage(PIL_image)
			self.preview.configure(image=tk_image)
			self.preview.image=tk_image
			self.pull_data(self.video_player.current_frame)
		except StopIteration:
			print("End of Video")
			self.more_image = False
		except TypeError:
			print("no video Loaded.")
			self.more_image = False

	def pull_data(self, frame_number):
		# First, need to find out where the trackers are located in the listbox (because of deleting)
		listbox_strs = self.tracker_list.get(0, END)
		print("finding where trackers are located", listbox_strs)
		tracker_map = {}
		for index in range(0, len(listbox_strs)):
			tracker_map[listbox_strs[index]] = index
		print("Map ", tracker_map)
		print("pull_data ", frame_number)
		data_entry = self.data[frame_number]
		next_previous = []
		for tracker_id, probabilities in data_entry.items():
			next_previous.append(str(tracker_id))
			try:
				mapped_index = tracker_map[str(tracker_id)]
			except KeyError:
				list_box_size = self.tracker_list.size()
				mapped_index = list_box_size
				tracker_map[tracker_id] = list_box_size
				print("Added map ", tracker_map)
			self.tracker_list.delete(mapped_index)
			self.tracker_list.insert(mapped_index, str(tracker_id))
			if probabilities[0]: 
				# print(probabilities[1][0])
				for i in range(0, len(self.emotion_lists)):
					list_box = self.emotion_lists[i]
					probability = probabilities[1][0][i]
					list_box.delete(mapped_index)
					formatted_number = "{:.1f}".format(probability*100)
					list_box.insert(mapped_index, formatted_number)
			else:
				print("Tracker, ",tracker_id, " not active.")
				for i in range(0, len(self.emotion_lists)):
					list_box = self.emotion_lists[i]
					list_box.delete(mapped_index)
					list_box.insert(mapped_index, str(0))
			try:
				self.previous_trackers.pop(self.previous_trackers.index(str(tracker_id)))
			except ValueError:
				print("tracker not present in previous frame")
		print("Previous Trackers: ", self.previous_trackers)
		for absent in self.previous_trackers:
			try:
				mapped_absent = tracker_map[absent] #shouldn't raise and exception since they've all been seen before by definition
			except KeyError:
				print("this error probably happened while merging :). Just restart, or go back a few frames, and things should be fine.")
				continue
			for i in range(0, len(self.emotion_lists)):
				list_box = self.emotion_lists[i]
				list_box.delete(mapped_absent)
				list_box.insert(mapped_absent, "NA")
		self.previous_trackers = next_previous

	def pause(self):
		self.more_image = False

	def reset_lists(self):
		self.tracker_list.delete(0, END)
		for list_box in self.emotion_lists:
			list_box.delete(0, END)

	def restart(self):
		#Save the state of original_on
		size_var = self.video_player.original_on
		del self.video_player
		self.previous_trackers = []
		self.reset_lists()
		self.video_player = VideoProcessor(self.video_file)
		self.video_player.original_on = size_var
		self.video_generator = self.video_player.read_video()
		self.show_preview()

	def play(self):
		self.more_image = True

	def add_functionality(self):
		self.master.bind("<Escape>", self.close_program)
		self.file_upload_button.configure(command=self.upload_file)
		self.video_play_button.configure(command=self.play)
		self.video_pause_button.configure(command=self.pause)
		self.video_restart_button.configure(command=self.restart)
		self.tracker_list.bind('<<ListboxSelect>>', self.update_stats)
		self.change_file_button.configure(command=self.download)
		self.video_prev_frame_button.configure(command=lambda:self.seek(-2))
		self.video_next_frame_button.configure(command=lambda:self.seek(0))
		# since current_frame is auto incremented, the seek arguments are like this :)
		self.delete_button.configure(command=lambda:self.delete_tracker(0))
		self.delete_before_button.configure(command=lambda:self.delete_tracker(self.video_player.current_frame, before=True))
		self.delete_after_button.configure(command=lambda:self.delete_tracker(self.video_player.current_frame, before=False))
		self.list_scroller.config(command=self.scroll_all)
		self.merge_button.configure(command=self.merge)
		self.original_button.configure(command=self.see_original)


	def see_original(self):
		self.video_player.original_on = not self.video_player.original_on

	def merge(self):
		indexs = self.tracker_list.curselection()
		new_id = ""
		mapped_indexs = []
		for i in indexs:
			value = self.tracker_list.get(i)
			mapped_indexs.append(value)
		print("Mapped Index = ", mapped_indexs)
		for i in mapped_indexs:
			new_id += str(i) + " "
		for frame in range(0, len(self.data)):
			for i in mapped_indexs:
				try:
					self.data[frame][new_id] = self.data[frame][i]
					print(new_id, " IN frame #", frame, " ", self.data[frame][new_id])
					del self.data[frame][i]
					print("Tracker #", str(i), "Being merged into ", new_id)
				except KeyError:
					pass
					# print("Tracker ", str(i), " not in frame ")
		self.sort_data()
		self.reset_lists()
		self.seek(-1)


	def scroll_all(self, *args):
		self.tracker_list.yview(*args)
		for box in self.emotion_lists:
			box.yview(*args)

	def seek(self, increment):
		self.video_player.current_frame += increment
		self.show_preview()

	def delete_tracker(self, boundary_frame, before=None):
		print("Before = ", before)
		print("boundary = ", boundary_frame)
		if before is None:
			boundary = range(0, len(self.data))
		else:
			if before:
				boundary = range(0, boundary_frame+1)
			else:
				boundary = range(boundary_frame+1, len(self.data))
		found_flag = False
		indexs = self.tracker_list.curselection()
		mapped_indexs = []
		for i in indexs:
			value = self.tracker_list.get(i)
			mapped_indexs.append(value)
		print("Mapped Index = ", mapped_indexs)
		for frame in boundary:
			for i in mapped_indexs:
				try:
					del self.data[frame][i]
					print("Tracker ", str(i), "deleted from frame #", frame)
				except KeyError:
					pass
					# print("Tracker ", str(i), " not found in frame #", frame, " to delete")

		self.sort_data()
		for i in reversed(indexs): #remove here, because in loop would cause MANY problems
			self.tracker_list.delete(i)
			for emotion_list in self.emotion_lists:
				emotion_list.delete(i)
		self.update_stats(None)

	def download(self):
		#Make one pass through self.data to get a list of all trackers
		field_names = ["Frame Number"]
		trackers = []
		for frame in self.data:
			keys = frame.keys()
			for key in keys:
				if not key in trackers:
					trackers.append(key)
		print(trackers)
		for tracker in trackers:
			for emotion in self.emotion_names:
				field_names.append("P" + str(tracker) + emotion)
		print("Field names = ", field_names)
		split_loc = self.video_file.split(".")
		csv_name = split_loc[0] + ".csv"
		with open(csv_name, 'w', newline='') as csv_file:
			writer = DictWriter(csv_file, fieldnames = field_names)
			writer.writeheader()
			frame_count = 1
			row = {}
			for frame in self.data:
				row["Frame Number"] = frame_count
				column_index = 1
				for tracker in trackers:
					# tracker = str(tracker)
					print("Tracker = ",tracker)
					print(frame)
					# try:
					# 	print(frame[tracker])
					# except KeyError:
					# 	print("NOPE")
					if (not tracker in frame) or (not frame[tracker][0]):
						print("tracker not found in frame")
						for i in range(0, 7):
							row[field_names[column_index]] = ""
							column_index += 1
					else:
						for i in range(0, 7):
							print(i, frame[tracker][1][0])
							print(column_index, field_names)
							row[field_names[column_index]] = frame[tracker][1][0][i]
							column_index += 1
				frame_count += 1
				pprint.pprint(row)
				writer.writerow(row)
				row = {}
		print("Download File successfully written.")

	def upload_file(self):
		location = askopenfilename(title="Choose File")
		if location == () or location == "": #User cancelled
			return
		self.video_file = location
		split_loc = location.split(".")
		self.data_file = split_loc[0] + ".data"
		print("data file, ", self.data_file)
		print("video_file ", self.video_file)
		self.video_player = VideoProcessor(self.video_file)
		self.video_generator = self.video_player.read_video()
		self.reset_lists()
		self.import_data()
		self.show_preview()


	def close_program(self, event):
		self.master.destroy()

	def place_gui(self):
		self.preview.grid(row=0, column=0, columnspan=10, rowspan=10)
		self.video_restart_button.grid(row=15, column=11)
		self.video_pause_button.grid(row=15, column=12)
		self.video_play_button.grid(row=15, column=13)
		self.video_next_frame_button.grid(row=15, column=15)
		self.video_prev_frame_button.grid(row=15, column=14)
		self.file_upload_button.grid(row=12, column=12)
		# self.save_file_label.grid(row=12, column=5, columnspan=2)
		self.change_file_button.grid(row=12, column=11)
		Label(self.emotion_frame, text="Tracker").grid(row=0, column=0)
		self.tracker_list.grid(row=1, column=0, rowspan=5)
		self.emotion_frame.grid(row=11, column=1, rowspan=5)
		# prob_font = ("Courier", 9)
		for i in range(1, len(self.emotion_lists)+1):
			# self.emotion_lists[i-1].config(font=prob_font)
			Label(self.emotion_frame, text=self.emotion_names[i-1]).grid(row=0, column=i)
			self.emotion_lists[i-1].grid(row=1, column=i)
		self.delete_before_button.grid(row=11, column=11)
		self.delete_after_button.grid(row=11, column=13)
		self.delete_button.grid(row=11, column=12)
		self.merge_button.grid(row=12, column=13)
		self.list_scroller.grid(row=13, column=11, sticky='NS')
		self.original_button.grid(row=13, column=12)
		self.frame_number_label.grid(row=13, column=13)
		Label(self.master, text="Full Video Report: ").grid(row=0, column=11)
		self.current_trackers.grid(row=0, column=12)
		Label(self.master, text="Total Frames: ").grid(row=1, column=11)
		self.total_frame_label.grid(row=1, column=12)
		Label(self.master, text="Tracker Uptime % ").grid(row=1, column=13)
		self.uptime_label.grid(row=1, column=14)
		index = 0
		row_index = 2
		try:
			while index < len(self.emotion_names):
				first_name = "% " + self.emotion_names[index]
				Label(self.master, text=first_name).grid(row=row_index, column=11)
				self.emotion_labels[index].grid(row=row_index, column=12)
				index += 1
				second_name = "% " + self.emotion_names[index]
				Label(self.master, text=second_name).grid(row=row_index, column=13)
				self.emotion_labels[index].grid(row=row_index, column=14)
				index += 1
				row_index += 1
		except IndexError:
			pass
		

	def initialize_preview(self):
		preview_image = Image.open("preview_start.jpg")
		resized_image = preview_image.resize((600, 400), Image.ANTIALIAS)
		tk_image = ImageTk.PhotoImage(resized_image)
		self.preview.configure(image = tk_image)
		self.preview.image=tk_image




if __name__ == "__main__":
	root = Tk()
	gui = GUI(root)
	# gui.mainloop()