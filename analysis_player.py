import cv2
from time import sleep
PREVIEW_WIDTH = 600
PREVIEW_HEIGHT = 400

class VideoProcessor():
	def __init__(self, video_file):
		self.preview_image = None
		self.raw_image = None
		self.capture = cv2.VideoCapture(video_file)
		self.current_frame = 0
		self.original_on = False
		

	def read_video(self):
		while self.capture.isOpened():
			sleep(.03)
			self.capture.set(1, self.current_frame)
			flag, self.raw_image = self.capture.read()
			if not flag:
				break
			self.preview_frame = self.raw_image
			if self.original_on:
				cv2.imshow('Original Size', self.preview_frame)
				if cv2.waitKey(1) & 0xFF == ord('q'):
					break
			else:
				try:
					cv2.destroyWindow('Original Size')
				except:
					pass #if there's no window to destroy, it will error (better to ask forgiveness...)
			self.preview_frame = cv2.cvtColor(self.preview_frame, cv2.COLOR_BGR2RGB)
			self.preview_frame = cv2.resize(self.preview_frame, (PREVIEW_WIDTH, PREVIEW_HEIGHT))
			yield self.preview_frame
			self.current_frame += 1

	def __del__(self):
		try:
			cv2.destroyAllWindows()
			self.capture.release()
			self.video_writer.release()
		except Exception:
			pass